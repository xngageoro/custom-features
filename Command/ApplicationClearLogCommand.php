<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Command;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\CronBundle\Command\CronCommandInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Doctrine\Persistence\ManagerRegistry;
use Xngage\CustomFeaturesBundle\Entity\ApplicationLog;

class ApplicationClearLogCommand extends Command implements CronCommandInterface
{
    /** @var string */
    protected static $defaultName = 'oro:cron:clean-logs-custom-application';

    private ConfigManager $configManager;
    private ManagerRegistry $doctrine;

    public function __construct(
        ManagerRegistry $doctrine,
        ConfigManager $configManager
    ) {
        $this->doctrine = $doctrine;
        $this->configManager = $configManager;

        parent::__construct();
    }

    public function getDefaultDefinition()
    {
        return $this->configManager->get('xngage_custom_features.application_log_job_time');
    }

    public function isActive()
    {
        $appLogJobDay = $this->configManager->get('xngage_custom_features.application_log_job_day');
        $appLogJobTime = $this->configManager->get('xngage_custom_features.application_log_job_time');

        return !empty($appLogJobDay) && !empty($appLogJobTime);
    }


    protected function configure()
    {
        $this->setDescription('Schedules to remove  application logs after number of days.')
            ->setHelp(
                <<<'HELP'
The <info>%command.name%</info> command job will clean up the old logs (< 7 days configurable ) from the custom application log.

HELP
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $this->doctrine->getManagerForClass(ApplicationLog::class)->getConnection();
        $appLogJobDay = $this->configManager->get('xngage_custom_features.application_log_job_day');
        $date = (new \DateTime())->modify("-$appLogJobDay day");
        $builder = $connection->createQueryBuilder()
                        ->delete('xngage_application_log')
                        ->where('created_at <= :time')
                        ->setParameter(':time', $date->format("Y-m-d H:i:s"));
        $builder->execute();
    }
}