<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Command;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\CronBundle\Command\CronCommandInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Doctrine\Persistence\ManagerRegistry;
use Xngage\CustomFeaturesBundle\Entity\ApplicationLog;

class ChmodCommand extends Command implements CronCommandInterface
{
    /** @var string */
    protected static $defaultName = 'oro:cron:chmod';

    public function getDefaultDefinition()
    {
        return '';
    }

    public function isActive()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return true;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //give it a chance until c:c done
        sleep(30);

        exec ("sudo find /var/www/html/xng.fgi.oro/var/ -type d -exec chmod 0777 {} +");//for sub directory
        exec ("sudo find /var/www/html/xng.fgi.oro/var/ -type f -exec chmod 0777 {} +");//for files inside directory

        exec ("sudo find /var/www/html/xng.fgi.oro/public/ -type d -exec chmod 0777 {} +");//for sub directory
        exec ("sudo find /var/www/html/xng.fgi.oro/public/ -type f -exec chmod 0777 {} +");//for files inside directory
    }
}