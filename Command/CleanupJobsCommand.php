<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
declare(strict_types=1);

namespace Xngage\Bundle\CustomFeaturesBundle\Command;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\CronBundle\Command\CronCommandInterface;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\MessageQueueBundle\Entity\Job;
use Oro\Component\MessageQueue\Job\Job as JobComponent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Clears old records from message_queue_job table.
 */
class CleanupJobsCommand extends Command implements CronCommandInterface
{
    /** @var string */
    protected static $defaultName = 'oro:cron:message-queue:cleanup-jobs';

    private DoctrineHelper $doctrineHelper;
    private ConfigManager $configManager;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        ConfigManager $configManager
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->configManager = $configManager;
        parent::__construct();
    }

    public function isActive()
    {
        return $this->configManager->get('xngage_custom_features.cleanup_history_job_status');
    }

    public function getDefaultDefinition()
    {
        return $this->configManager->get('xngage_custom_features.cleanup_history_job_time');
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    public function configure()
    {
        $this
            ->addOption(
                'dry-run',
                'd',
                InputOption::VALUE_NONE,
                'Show the number of jobs that match the cleanup criteria instead of deletion'
            )
            ->setDescription('Clears old records from message_queue_job table.')
            ->setHelp(
                <<<'HELP'
The <info>%command.name%</info> command clears successful job records
that are older than 2 weeks and failed job records older than 1 month
from <comment>message_queue_job</comment> table.

  <info>php %command.full_name%</info>

The <info>--dry-run</info> option can be used to show the number of jobs that match
the cleanup criteria instead of deleting them:

  <info>php %command.full_name% --dry-run</info>

HELP
            )
            ->addUsage('--dry-run')
        ;
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('dry-run')) {
            $output->writeln(
                sprintf(
                    '<info>Number of jobs that would be deleted: %d</info>',
                    $this->countRecords()
                )
            );

            return;
        }

        $output->writeln(sprintf(
            '<comment>Number of jobs that has been deleted:</comment> %d',
            $this->deleteRecords()
        ));

        $output->writeln('<info>Message queue job history cleanup complete</info>');
    }

    /**
     * @return mixed
     */
    private function deleteRecords()
    {
        $qb = $this->getEntityManager()
            ->getRepository(Job::class)
            ->createQueryBuilder('job');
        $qb->delete(Job::class, 'job');
        $this->addOutdatedJobsCriteria($qb);

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    private function countRecords()
    {
        $qb = $this->getEntityManager()
            ->getRepository(Job::class)
            ->createQueryBuilder('job');
        $qb->select('COUNT(job.id)');
        $this->addOutdatedJobsCriteria($qb);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->doctrineHelper->getEntityManagerForClass(Job::class);
    }

    private function addOutdatedJobsCriteria(QueryBuilder $qb): void
    {
        $qb
            ->where($qb->expr()->andX(
                $qb->expr()->eq('job.status', ':status_running'),
                $qb->expr()->lt('job.createdAt', ':running_created_time')
            ))
            ->setParameter('status_running', JobComponent::STATUS_RUNNING)
            ->setParameter(
                'running_created_time',
                new \DateTime($this->getIntervalForRunning(), new \DateTimeZone('UTC')),
                Types::DATETIME_MUTABLE
            );
    }

    private function getIntervalForRunning(): string
    {
        $days = $this->configManager->get('xngage_custom_features.cleanup_history_job_day');
        return sprintf('%d days', $days);
    }
}
