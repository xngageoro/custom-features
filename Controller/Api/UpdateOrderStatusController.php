<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Controller\Api;

use Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue;
use Oro\Bundle\EntityExtendBundle\Tools\ExtendHelper;
use Oro\Bundle\OrderBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Extend\Entity\EV_Order_Internal_Status;

class UpdateOrderStatusController extends AbstractController
{
    protected $em;
    protected $registry;

    public function __construct($em, $registry)
    {
        $this->em = $em;
        $this->registry = $registry;
    }

    /**
     * Update order internal status for a specific record.
     *
     * @param Request $request
     *
     * @ApiDoc(
     *     resource=true,
     *     description="Update order status",
     *     views={"rest_json_api"},
     *     section="updateorderstatus",
     *     requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "requirement"="\d+",
     *              "description"="The 'id' requirement description."
     *          },
     *          {
     *              "name"="status",
     *              "dataType"="string",
     *              "requirement"="\s+",
     *              "description"="order status: open, close, cancelled, shipped ..."
     *          }
     *     },
     *     output={
     *          "class"="Your\Namespace\Class",
     *          "fields"={
     *              {
     *                  "name"="aField",
     *                  "dataType"="string",
     *                  "description"="The 'aField' field description."
     *              }
     *          }
     *     },
     *     statusCodes={
     *          200="Returned when successful",
     *          500="Returned when an unexpected error occurs"
     *     }
     * )
     *
     * @return Response
     */
    public function updateOrderStatusAction(Request $request)
    {
        $orderId = $request->request->get('id');
        $status = $request->request->get('status');

        if ($orderId == '') {
            $error = [
                "errors" => [
                    "status" => "400",
                    "detail" => "OrderId can not be empty!",
                ]
            ];

            return new Response(json_encode($error),Response::HTTP_BAD_REQUEST);
        }

        if ($status == '') {
            $error = [
                "errors" => [
                    "status" => "400",
                    "detail" => "Status can not be empty!",
                ]
            ];

            return new Response(json_encode($error),Response::HTTP_BAD_REQUEST);
        }

        $order = $this->em->getRepository(Order::class)->findOneById($orderId);

        if (!$order) {
            $error = [
                "errors" => [
                    "status" => "404",
                    "detail" => "Order can not be found!",
                ]
            ];

            return new Response(json_encode($error),Response::HTTP_NOT_FOUND);
        }

        //get all available order statuses
        $orderStatuses = $this->em->getRepository(EV_Order_Internal_Status::class)->findAll();

        foreach ($orderStatuses as $orderStatus) {
            $statusArray[] = strtolower($orderStatus->getName());
        }

        if ( !in_array($status, $statusArray)) {
            $error = [
                "errors" => [
                    "status" => "404",
                    "detail" => "Status ($status) should be one of: (" . implode(',' , $statusArray) . ')',
                ]
            ];

            return new Response(json_encode($error),Response::HTTP_NOT_FOUND);
        }

        $order->setInternalStatus($this->getInternalStatus($status));
        $this->em->persist($order);
        $this->em->flush();

        $success = [
            "success" => [
                "status" => "200",
                "detail" => 'Order status changed!',
            ]
        ];

        return new Response(json_encode($success), Response::HTTP_OK);
    }

    /**
     * @param string $statusId
     *
     * @return object|AbstractEnumValue
     */
    protected function getInternalStatus($statusId)
    {
        $className = ExtendHelper::buildEnumValueClassName(Order::INTERNAL_STATUS_CODE);

        return $this->registry->getManagerForClass($className)->getRepository($className)->find($statusId);
    }
}