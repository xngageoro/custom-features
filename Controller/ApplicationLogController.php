<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Xngage\CustomFeaturesBundle\Entity\ApplicationLog;
use Xngage\CustomFeaturesBundle\Form\ApplicationLogType;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;

/**
 * Controller provide information about custom application logs
 *
 */
class ApplicationLogController extends AbstractController
{
    /**
     * @Route("/list", name="application_log_list")
     * @Template
     * @AclAncestor("oro_datagrid_gridview_view")
     *
     * @return array
     */
    public function applicationLogsAction()
    {
        return [
        ];
    }

    /**
     * @Route("/view/{id}", name="application_log_view", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("oro_datagrid_gridview_view")
     * @param ApplicationLog $applicationLog
     *
     * @return array
     */
    public function viewAction(ApplicationLog $applicationLog)
    {
        return [
            'entity' => $applicationLog,
        ];
    }
}
