<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Controller;

use Oro\Bundle\CronBundle\Engine\CommandRunnerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class CommandsToolController extends AbstractController
{
    const CLEAN_CACHE_COMMAND = 'cache-clean';
    const CREATE_WEBSITE_INDEXES = 'create-website-indexes';
    const CHMOD_COMMAND = 'chmod';
    const RELOAD_DEFINITION_COMMAND = 'reload-definitions';
    const CLEAN_UP_JOBS = 'clean-up-jobs';
    const CLEAN_UP_PRODUCT = 'clean-up-product';
    const CLEAN_UP_INVENTORY = 'clean-up-inventory';
    const CLEAN_UP_QUOTE = 'clean-up-quote';
    const CLEAN_UP_ORDER = 'clean-up-order';

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var CommandRunnerInterface
     */
    private $commandRunner;

    public function __construct(
        KernelInterface        $kernel,
        CommandRunnerInterface $commandRunner
    ) {
        $this->kernel = $kernel;
        $this->commandRunner = $commandRunner;
    }

    /**
     * @Route("/", name="on_demand_commands_tool_index")
     * @Template
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMINISTRATOR');

        if ($request->isMethod('POST')) {
            $command = $request->get('command');
            switch ($command) {
                case self::CLEAN_CACHE_COMMAND:
                    $message = 'Cache has been cleaned successfully';
                    $this->cleanCache($request, $message);
                    break;
                case self::CREATE_WEBSITE_INDEXES:
                    $message = 'Create website indexes command has been run successfully';
                    $this->createWebsiteIndexes($request, $message);
                    break;
                case self::CHMOD_COMMAND:
                    $message = 'Chmod command has been run successfully';
                    $this->chmodFolders($request, $message);
                    break;
                case self::RELOAD_DEFINITION_COMMAND:
                    $message = 'Reload Cron job definition command has been run successfully';
                    $this->reloadCronJobDefinition($request, $message);
                    break;
                case self::CLEAN_UP_JOBS:
                    $message = 'Old running jobs have been deleted successfully';
                    $this->cleanupJobs($request, $message);
                    break;
                case self::CLEAN_UP_PRODUCT:
                    $message = 'Clean up products has been run successfully';
                    $this->cleanupProducts($request, $message);
                    break;
                case self::CLEAN_UP_INVENTORY:
                    $message = 'Clean up inventory has been run successfully';
                    $this->cleanupInventory($request, $message);
                    break;
                case self::CLEAN_UP_QUOTE:
                    $message = 'Clean up quotes has been run successfully';
                    $this->cleanupQuotes($request, $message);
                    break;
                case self::CLEAN_UP_ORDER:
                    $message = 'Clean up orders has been run successfully';
                    $this->cleanupOrders($request, $message);
                    break;
            }
        }

        return [
            'commands' => [
                ['title' => 'Cache Clean', 'value' => self::CLEAN_CACHE_COMMAND],
                ['title' => 'Create website indexes', 'value' => self::CREATE_WEBSITE_INDEXES],
                ['title' => 'Chmod var & public folders', 'value' => self::CHMOD_COMMAND],
                ['title' => 'Reload Cron job definition', 'value' => self::RELOAD_DEFINITION_COMMAND],
                ['title' => 'Clean up old running jobs', 'value' => self::CLEAN_UP_JOBS],
                ['title' => 'Clean-up Products', 'value' => self::CLEAN_UP_PRODUCT],
                ['title' => 'Clean-up Inventory', 'value' => self::CLEAN_UP_INVENTORY],
                ['title' => 'Clean-up Quotes', 'value' => self::CLEAN_UP_QUOTE],
                ['title' => 'Clean-up Orders', 'value' => self::CLEAN_UP_ORDER],
            ]
        ];
    }

    private function chmodFolders($request, $message)
    {
        try {
            $this->commandRunner->run('oro:cron:chmod', []);

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function cleanCache($request, $message)
    {
        try {
            $this->commandRunner->run(
                'cache:clear',
                ['--env' => $this->kernel->getEnvironment()]
            );

            // the supervisrod run 2 program as same time (parallel) so if we add c:c and chmod
            // the first program will consum the c:c and the second will consume chmod at same time
            // but we need to run chmod after c:c finish so we have to add another chmod to run
            // after c:c finish
            $this->commandRunner->run('oro:cron:chmod', []);

            //this will be in queue until c:c finish
            $this->commandRunner->run('oro:cron:chmod', []);

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function createWebsiteIndexes($request, $message)
    {
        try {
            $this->commandRunner->run(
                'oro:website-elasticsearch:create-website-indexes',
                ['--env' => $this->kernel->getEnvironment()]
            );

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function reloadCronJobDefinition($request, $message)
    {
        try {
            $this->commandRunner->run('oro:cron:definitions:load', ['--env' => $this->kernel->getEnvironment()]);

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function cleanupJobs($request, $message)
    {
        try {
            $this->commandRunner->run(
                'oro:cron:message-queue:cleanup-jobs',
                ['--env' => $this->kernel->getEnvironment()]
            );

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function cleanupProducts($request, $message)
    {
        try {
            $this->commandRunner->run(
                'oro:cron:clean-up-product',
                ['--disabled-listeners' => ['all']]
            );

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function cleanupInventory($request, $message)
    {
        try {
            $this->commandRunner->run(
                'oro:cron:clean-up-inventory',
                ['--disabled-listeners' => ['all']]
            );

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function cleanupQuotes($request, $message)
    {
        try {
            $this->commandRunner->run(
                'oro:cron:clean-up-quote',
                ['--disabled-listeners' => ['all']]
            );

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }

    private function cleanupOrders($request, $message)
    {
        try {
            $this->commandRunner->run(
                'oro:cron:clean-up-order',
                ['--disabled-listeners' => ['all']]
            );

            $request->getSession()->getFlashBag()->add('success', $message);
        } catch (\Exception $exception) {
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
        }
    }
}