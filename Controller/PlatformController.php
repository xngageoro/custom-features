<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Xngage\CustomFeaturesBundle\Form\ApplicationLogType;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;

/**
 * Controller provide information about logs file
 *
 * @Route("/platform")
 */
class PlatformController extends AbstractController
{
    const NUMBER_OF_LINES = 1000;

    /**
     * @Route("/logs/development", name="xngage_platform_system_dev_logs")
     * @Template("@XngageCustomFeatures/Platform/systemlogs.html.twig")
     * @AclAncestor("oro_datagrid_gridview_view")
     */
    public function systemDevLogsAction()
    {
        return $this->getLogsFroEnvironment('dev');
    }

    /**
     * @Route("/logs/production", name="xngage_platform_system_prod_logs")
     * @Template("@XngageCustomFeatures/Platform/systemlogs.html.twig")
     * @AclAncestor("oro_datagrid_gridview_view")
     */
    public function systemProdLogsAction()
    {
        return $this->getLogsFroEnvironment('prod');
    }
    
    /**
     * @param $environment
     * @return array
     */
    private function getLogsFroEnvironment($environment)
    {
        $logs = [];
        $error = null;
        $filePath = $this->get('kernel')->getLogDir() . "/$environment.log";
        try {
            $file = new \SplFileObject($filePath, 'r');
            $file->seek(PHP_INT_MAX);
            $lastLine = $file->key();
            $offset = $lastLine - self::NUMBER_OF_LINES > 0 ? $lastLine - self::NUMBER_OF_LINES : 0;
            $lines = new \LimitIterator($file, $offset, $lastLine);
            $logs = iterator_to_array($lines);
            // To close the handler
            $filePath = null;
        } catch (\RuntimeException $exception) {
            $error = $this->get('translator')->trans('xngage.platform.system_logs.error');
        }

        return [
            'env' => $environment,
            'error' => $error,
            'logLines' => $logs,
        ];
    }
}
