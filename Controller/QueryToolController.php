<?php

namespace Xngage\Bundle\CustomFeaturesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class QueryToolController extends Controller
{
    const DEFUALT_LIMIT_PER_PAGE = 20;

    /**
     * @Route("/query-tool", name="query_tool")
     * @Template()
     * @Acl(
     *     id="query_tool",
     *     type="action"
     * )
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMINISTRATOR');

        $queryResult = [];
        $query = '';
        $pagination = '';
        $limit = '';

        if($request->isMethod('POST')){
            $query = $request->request->get('query');
            $limit = $request->request->get('querylimit');

            $queryResult = $this->getQueryResult($request, $query);

            if (!$limit) {
                $limit = self::DEFUALT_LIMIT_PER_PAGE;
            }
            $total = count($queryResult);

            $numberOfPages = ceil($total / $limit);

            $currentPage = min($numberOfPages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
                'options' => array(
                    'default'   => 1,
                    'min_range' => 1,
                ),
            )));

            $offset = ($currentPage - 1)  * $limit;
            $start = $offset + 1;
            $end = min(($offset + $limit), $total);

            $pagination = $this->getPaginationHtml($currentPage, $numberOfPages, $start, $end, $total);

            $queryResult = array_slice($queryResult, $start - 1, $limit);
        }

        return array('currentTime' => date('Y-m-d H:i:s'),'result' => $queryResult, 'query' => $query, 'pagination' => $pagination, 'limit' => $limit);
    }

    /**
     * @param $page
     * @return mixed
     */
    public function getPageUrl($page) {
        $pageUrl = $this->container->get('router')->generate('query_tool', ['page' => $page]);
        
        return $pageUrl;
    }

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function getQueryResult($request, $query) {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $result = $em->getConnection()->query($query);

        $queryResult = $result->fetchAll();

        return $queryResult;
    }

    /**
     * @param $currentPage
     * @param $numberOfPages
     * @param $start
     * @param $end
     * @param $total
     * @return string
     */
    public function getPaginationHtml($currentPage, $numberOfPages, $start, $end, $total) {
        // The "back" link
        $prevlink = ($currentPage > 1) ? '<a data-page-id = "1" href="'. $this->getPageUrl(1) .'" title="First page">&laquo;</a> <a data-page-id="'. $currentPage - 1 .'" href="' . $this->getPageUrl($currentPage - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';

        // The "forward" link
        $nextlink = ($currentPage < $numberOfPages) ? '<a data-page-id="'. $currentPage + 1 .'" href="' . $this->getPageUrl($currentPage + 1) . '" title="Next page">&rsaquo;</a> <a data-page-id="'. $numberOfPages .'" href="' . $this->getPageUrl($numberOfPages) . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';

        // Display the paging HTML information
        $pagination = '<div id="query-tool-pagination"><p>' . $prevlink . ' Page ' . $currentPage . ' of '. $numberOfPages . ' pages, displaying ' . $start. '-' . $end . ' of ' . $total . ' results ' . $nextlink;
        $pagination .= ' | Go to page: ';
        $pagination .= '<input type="text" name="query_page_number" value="'. $currentPage .'"/>';
        $pagination .= '<input type="submit" id="query-tool-go-page" value="GO" data-url="'. $this->container->get('router')->generate('query_tool') .'"/>';

        $pagination .= '</p></div>';

        return $pagination;
    }
}