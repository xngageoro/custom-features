<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Controller;

use Oro\Bundle\CronBundle\Engine\CommandRunnerInterface;
use Oro\Bundle\ProductBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;

/**
 * Controller for reindex products
 *
 */
class ReindexProductController extends AbstractController
{
    protected $reindexManager;
    protected $em;
    private CommandRunnerInterface $commandRunner;

    public function __construct($reindexManager, $em, $commandRunner)
    {
        $this->reindexManager = $reindexManager;
        $this->em = $em;
        $this->commandRunner = $commandRunner;
    }

    /**
     * @Route("/", name="reindex_products")
     * @Template
     * @AclAncestor("oro_datagrid_gridview_view")
     * @return array
     */
    public function reindexProductAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMINISTRATOR');

        if ($request->isMethod('POST')) {
            $skus = $request->request->get('skus');

            if (trim($skus) != '') {
                $skusArr = explode(',', $skus);

                $productIds = [];

                foreach ($skusArr as $sku) {
                    $sku = trim($sku);

                    $product = $this->em->getRepository(Product::class)->findOneBySku($sku);

                    if (!$product){
                        $productMeta = $this->em->getClassMetadata(Product::class);

                        if ($productMeta->hasField('custom_part_number')) {
                            $product = $this->em->getRepository(Product::class)->findOneBy(['custom_part_number' => $sku]);
                        }
                    }

                    if ($product) {
                        $productIds[] = $product->getId();
                    }
                }

                if ( count($productIds) ) {
                    $this->reindexManager->reindexProducts($productIds);
                }
            }
            else{
                $this->commandRunner->run('oro:cron:indexing-products');
            }

            return ['success' => 'Reindex process is scheduled'];
        }

        return ['success' => ''];
    }
}
