<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Controller;

use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\TranslationBundle\Entity\TranslationKey;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;

/**
 * Controller for Uom
 *
 */
class UomController extends AbstractController
{
    protected $em;

    const UOM_TRANSLATION_KEY = [
        ['oro.product_unit.%s.label.full', 'messages', '*uom*'],
        ['oro.product_unit.%s.label.full_plural', 'messages', '*uom*'],
        ['oro.product_unit.%s.label.short', 'messages', '*uom*'],
        ['oro.product_unit.%s.label.short_plural', 'messages', '*uom*'],
        ['oro.product_unit.%s.value.full', 'messages', '{0} none|]-3000,1] %count% *uom*|]1,Inf] %count% *uom*'],
        ['oro.product_unit.%s.value.full_fraction', 'messages', '%formattedCount% *uom*'],
        ['oro.product_unit.%s.value.full_fraction_gt_1', 'messages', '%formattedCount% *uom*'],
        ['oro.product_unit.%s.value.short', 'messages', '{0} none|]-3000,1] %count% *uom*|]1,Inf] %count% *uom*'],
        ['oro.product_unit.%s.value.short_fraction', 'messages', '%formattedCount% *uom*'],
        ['oro.product_unit.%s.value.short_fraction_gt_1', 'messages', '%formattedCount% *uom*'],
        ['oro.product.product_unit.%s.label.full', 'jsmessages', '*uom*'],
        ['oro.product.product_unit.%s.label.full_plural', 'jsmessages', '*uom*'],
        ['oro.product.product_unit.%s.label.short', 'jsmessages', '*uom*'],
        ['oro.product.product_unit.%s.label.short_plural', 'jsmessages', '*uom*'],
        ['oro.product.product_unit.%s.value.full', 'jsmessages', '{0} none|]-3000,1] %count% *uom*|]1,Inf] %count% *uom*'],
        ['oro.product.product_unit.%s.value.short', 'jsmessages', '{0} none|{1} {{ count }} *uom*|]1,Inf]{{ count }} *uom*'],
        ['oro.product.product_unit.%s.value.label', 'jsmessages', '{0} none|{1} *uom*|]1,Inf] *uom*']
    ];

    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/delete/{uom}", name="delete_uom")
     * @Layout()
     * @return array
     */
    public function deleteUomAction($uom)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMINISTRATOR');

        //delete uom itself
        $unit = $this->em->getRepository(ProductUnit::class)->findOneByCode($uom);
        if ($unit) {
            $this->em->remove($unit);
            $this->em->flush();
        }

        foreach (self::UOM_TRANSLATION_KEY as $transKey) {
            $key = sprintf($transKey[0], $uom);

            $transKeyObj = $this->em->getRepository(TranslationKey::class)->findOneByKey($key);

            if ($transKeyObj) {
                $this->em->remove($transKeyObj);
                $this->em->flush();
            }
        }

        return new Response();
    }

    /**
     * @Route("/add", name="add_uom")
     * @Template
     * @AclAncestor("oro_datagrid_gridview_view")
     * @return array
     */
    public function addUomAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMINISTRATOR');

        if ($request->isMethod('POST')) {
            $unit = trim($request->request->get('uom'));
            $unit = strtolower($unit);

            if ($unit != '') {
                $valid1 = $this->addUom($unit);
                
                $valid = $this->addTraslationKey($unit);

                if ($valid && $valid1) {
                    $uoms = $this->getAllUoms();
                    return ['success' => 'New Uom has been added successfully!', 'uoms' => $uoms];
                } else {
                    $uoms = $this->getAllUoms();
                    return ['success' => 'Uom is already exists!', 'uoms' => $uoms];
                }
            }
            else{
                $uoms = $this->getAllUoms();
                return ['success' => 'Uom can not be empty!', 'uoms' => $uoms];
            }
        }

        $uoms = $this->getAllUoms();
        return ['success' => '', 'uoms' => $uoms];
    }

    private function addUom($uom){
        $unit = $this->em->getRepository(ProductUnit::class)->findOneByCode($uom);

        $valid = false;

        if (!$unit) {
            $unit = new ProductUnit();
            $unit->setCode($uom)->setDefaultPrecision(0);
            $this->em->persist($unit);
            $this->em->flush();

            $valid = true;
        }

        return $valid;
    }

    private function addTraslationKey($uom) {
        $i = 0;
        foreach (self::UOM_TRANSLATION_KEY as $transKey) {
            $key = sprintf($transKey[0], $uom);
            $domain = $transKey[1];
            $value = str_replace('*uom*', $uom, $transKey[2]);

            $transKeyObj = $this->em->getRepository(TranslationKey::class)->findOneByKey($key);

            if (!$transKeyObj) {
                $transKeyObj = new TranslationKey();
                $transKeyObj->setKey($key);
                $transKeyObj->setDomain($domain);

                $this->em->persist($transKeyObj);
                $this->em->flush();

                $id = $transKeyObj->getId();

                //add value
                $query = "insert into oro_translation (translation_key_id, language_id, value, scope) values ('$id','1','$value','0')";
                $this->em->getConnection()->executeQuery($query);

                $i++;
            }
        }

        return ($i == count(self::UOM_TRANSLATION_KEY));
    }

    private function getAllUoms(){
        $query = "select oro_translation_key.id,key,value from oro_translation_key 
                  inner join oro_translation on oro_translation_key.id = oro_translation.translation_key_id
                  where key like 'oro.product\_unit%' and key like '%label.full' and value != ''
                  order by value";
        $result = $this->em->getConnection()->executeQuery($query);
        $uoms = $result->fetchAll();

        return $uoms;
    }
}
