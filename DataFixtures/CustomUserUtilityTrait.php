<?php
namespace Xngage\Bundle\CustomFeaturesBundle\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Oro\Bundle\UserBundle\Entity\User;

trait CustomUserUtilityTrait
{
    /**
     * @param ObjectManager $manager
     * @return User
     * @throws \LogicException
     */
    protected function getDefaultUser(ObjectManager $manager)
    {
        $users = $manager->getRepository('OroUserBundle:User')->findOneBy(['custom_default' => true]);

        if (!$users) {
            throw new \LogicException('There are no users in system');
        }

        return $users;
    }
}