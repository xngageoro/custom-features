<?php
#region copyright
/*
3 * XNGAGE CONFIDENTIAL
4 * __________________________
5 * 
6 * Copyright (C) 2021 Xngage - All Rights Reserved
7 * 
8 * All code or information contained herein is, and remains the 
9 * property of Xngage LLC and its customers.  The intellectual 
10 * and technical concepts contained are proprietary to Xngage LLC 
11 * and may be covered by U.S. and Foreign Patents, patents in 
12 * process, and are protected by trade secret or copyright law.  
13 * Dissemination of this information or reproduction of this material 
14 * is strictly forbidden unless prior written permission is obtained 
15 * from Xngage LLC.
16 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers swiftmailer.plugin.xngage_testing_emails.abstract for all mailer services
 */
class SwiftMailerHandlerPass implements CompilerPassInterface
{
    const SWIFTMAILER_MAILERS = 'swiftmailer.mailers';

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasParameter(self::SWIFTMAILER_MAILERS)) {
            $mailers = $container->getParameter(self::SWIFTMAILER_MAILERS);
            foreach ($mailers as $name => $mailer) {
                $this->configureMailerDecorator($name, $mailer, $container);
            }
        }
    }

    /**
     * @param string           $name
     * @param string           $mailer
     * @param ContainerBuilder $container
     */
    protected function configureMailerDecorator(
        $name,
        $mailer,
        ContainerBuilder $container
    ) {
        $pluginName = sprintf('swiftmailer.mailer.%s.plugin.xngage_testing_emails', $name);

        $childDefinition = new ChildDefinition('swiftmailer.plugin.xngage_testing_emails.abstract');
        $container->setDefinition($pluginName, $childDefinition);

        $container->getDefinition($mailer)
            ->addMethodCall('registerPlugin', [new Reference($pluginName)]);
    }
}
