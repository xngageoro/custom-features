<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    const ROOT_NODE = XngageCustomFeaturesExtension::ALIAS;
    const ALLOWED_GUEST_PAGES = 'allowed_guest_pages';
    const SAVE_APPLICATION_LOG = 'save_to_application_log';
    const APPLICATION_LOG_JOB_TIME = 'application_log_job_time';
    const APPLICATION_LOG_JOB_DAY = 'application_log_job_day';
    const LOGO_FIELD = 'logo_field';
    const PREFIX_FAVICONS_FIELD = 'favicon_%d_X_%d_field';
    const FAVICONS_SIZES = [57, 60, 72, 76, 114, 144, 120, 152, 180, 32, 192, 96, 16, 144];
    const TESTING_EMAIL_FIELD = 'testing_email';
    const CORPORATE_ADDRESS_STREET = 'corporate_address_street';
    const CORPORATE_ADDRESS_STREET_2 = 'corporate_address_street_2';
    const CORPORATE_SALE_REP_NAME = 'sale_rep_name';
    const CORPORATE_SALE_REP_EMAIL = 'sale_rep_email';
    const CORPORATE_ADDRESS_PHONE = 'corporate_address_phone';
    const CORPORATE_ADDRESS_FAX = 'corporate_address_fax';
    const CORPORATE_ADDRESS_EMAIL = 'corporate_address_email';
    const PRINT_TO_CONSOLE = 'print_to_console';
    const HOMEPAGE_SHOW_FEATURED_CATEGORIES = 'homepage_show_featured_categories';
    const HOMEPAGE_SHOW_FEATURED_PRODUCTS = 'homepage_show_featured_products';
    const HOMEPAGE_SHOW_NEW_ARRIVALS = 'homepage_show_new_arrivals';
    const HOMEPAGE_SHOW_TOP_SELLING_ITEMS = 'homepage_show_top_selling_items';
    const CLEAN_UP_JOBS_TIME = 'clean_up_job_time';
    const CLEAN_UP_JOBS_DAY = 'clean_up_job_day';
    const ORDER_CLEAN_UP_JOBS_DAY = 'order_clean_up_job_day';
    const CANCELLED_ORDER_CLEAN_UP_JOBS_DAY = 'cancelled_order_clean_up_job_day';
    const CLEANUP_HISTORY_JOB_STATUS = 'cleanup_history_job_status';
    const CLEANUP_HISTORY_JOB_TIME = 'cleanup_history_job_time';
    const CLEANUP_HISTORY_JOB_DAY = 'cleanup_history_job_day';
    const BIG_BATCH_SIZE = 'big_data_source_batch_size';
    const SMALL_BATCH_SIZE = 'small_data_source_batch_size';
    const BATCH_DUMPING_ENABLED = 'batch_dumping_enabled';
    const SEND_JOB_RESULT_EMAIL = 'send_job_result_email';
    const INDEXING_PRODUCTS_TIME = 'indexing_products_time';
    const SESSION_COOKIE_LIFETIME = 'session_cookie_lifetime_field';
    const SESSION_COOKIE_LIFETIME_ENABLE = 'session_cookie_lifetime_enable_field';

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root(static::ROOT_NODE);
        SettingsBuilder::append(
            $rootNode,
            [
                static::ALLOWED_GUEST_PAGES => ['value' => '', 'type' => 'string'],
                static::TESTING_EMAIL_FIELD => ['value' => "", 'type' => 'text'],
                static::SAVE_APPLICATION_LOG => ['value' => '', 'type' => 'boolean'],
                static::APPLICATION_LOG_JOB_TIME => ['value' => '0 0 * * *', 'type' => 'string'],
                static::APPLICATION_LOG_JOB_DAY => ['value' => '7', 'type' => 'string'],
                static::LOGO_FIELD => ['value' => '', 'type' => 'string'],
                static::CORPORATE_ADDRESS_STREET => ['value' => '', 'type' => 'string'],
                static::CORPORATE_ADDRESS_STREET_2 => ['value' => '', 'type' => 'string'],
                static::CORPORATE_SALE_REP_NAME => ['value' => '', 'type' => 'string'],
                static::CORPORATE_SALE_REP_EMAIL => ['value' => '', 'type' => 'string'],
                static::CORPORATE_ADDRESS_PHONE => ['value' => '', 'type' => 'string'],
                static::CORPORATE_ADDRESS_FAX => ['value' => '', 'type' => 'string'],
                static::CORPORATE_ADDRESS_EMAIL => ['value' => '', 'type' => 'string'],
                static::PRINT_TO_CONSOLE => ['value' => '', 'type' => 'boolean'],
                static::HOMEPAGE_SHOW_FEATURED_CATEGORIES => ['value' => true, 'type' => 'boolean'],
                static::HOMEPAGE_SHOW_FEATURED_PRODUCTS => ['value' => true, 'type' => 'boolean'],
                static::HOMEPAGE_SHOW_NEW_ARRIVALS => ['value' => true, 'type' => 'boolean'],
                static::HOMEPAGE_SHOW_TOP_SELLING_ITEMS => ['value' => false, 'type' => 'boolean'],
                static::CLEAN_UP_JOBS_TIME => ['value' => '*/60 * * * *', 'type' => 'string'],
                static::CLEAN_UP_JOBS_DAY => ['value' => '3', 'type' => 'string'],
                static::ORDER_CLEAN_UP_JOBS_DAY => ['value' => '3', 'type' => 'string'],
                static::CANCELLED_ORDER_CLEAN_UP_JOBS_DAY => ['value' => '365', 'type' => 'string'],
                static::CLEANUP_HISTORY_JOB_STATUS => ['value' => false, 'type' => 'boolean'],
                static::CLEANUP_HISTORY_JOB_TIME => ['value' => 'weekly', 'type' => 'string'],
                static::CLEANUP_HISTORY_JOB_DAY => ['value' => -7, 'type' => 'integer'],
                static::BIG_BATCH_SIZE => ['value' => '50000', 'type' => 'string'],
                static::SMALL_BATCH_SIZE => ['value' => '12000', 'type' => 'string'],
                static::BATCH_DUMPING_ENABLED => ['value' => false, 'type' => 'boolean'],
                static::SEND_JOB_RESULT_EMAIL => ['value' => '', 'type' => 'string'],
                static::INDEXING_PRODUCTS_TIME => ['value' => '0 10 * * *', 'type' => 'string'],
                static::SESSION_COOKIE_LIFETIME => ['value' => '120', 'type' => 'string'],
                static::SESSION_COOKIE_LIFETIME_ENABLE => ['value' => true, 'type' => 'boolean']
            ] + $this->getFaviconsFieldsName()
        );
        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }

    /**
     * return favicons fields name
     *
     * @return array
     */
    private function getFaviconsFieldsName()
    {
        $results = [];

        foreach (static::FAVICONS_SIZES as $size) {
            $name = sprintf(static::PREFIX_FAVICONS_FIELD, $size, $size);
            $results[$name] = ['value' => '', 'type' => 'string'];
        }

        return $results;
    }
}
