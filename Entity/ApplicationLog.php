<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion

namespace Xngage\Bundle\CustomFeaturesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Xngage\CustomFeaturesBundle\Model\ExtendApplicationLog;

/**
 * ApplicationLog
 *
 * @ORM\Table(
 *     name="xngage_application_log",
 *     indexes={
 *          @ORM\Index(name="idx_xngage_application_log_created_at", columns={"created_at"}),
 *          @ORM\Index(name="idx_xngage_application_log_type", columns={"type"}),
 *          @ORM\Index(name="idx_xngage_application_log_source", columns={"source"}),
 *          @ORM\Index(name="idx_xngage_application_log_title", columns={"title"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="Xngage\CustomFeaturesBundle\Entity\Repository\ApplicationLogRepository")
 *
 * @Config(
 *      routeName="application_log_list",
 *      routeView="application_log_view",
 *      defaultValues={
 *          "dataaudit"={
 *              "auditable"=true
 *          }
 *      }
 * )
 */
class ApplicationLog extends ExtendApplicationLog
{
    const ALERT_TYPE        = 'alert';
    const CRITICAL_TYPE     = 'critical';
    const DEBUG_TYPE        = 'debug';
    const EMERGENCY_TYPE    = 'emergency';
    const ERROR_TYPE        = 'error';
    const INFO_TYPE         = 'info';
    const NOTICE_TYPE       = 'notice';
    const WARNING_TYPE      = 'warning';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string")
     */
    protected $source;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    protected $message;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return ApplicationLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ApplicationLog
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return ApplicationLog
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ApplicationLog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
    */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ApplicationLog
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}