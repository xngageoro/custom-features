<?php
#region copyright
/**
 * XNGAGE CONFIDENTIAL
 * __________________________
 *  
 * Copyright (C) 2021 Xngage - All Rights Reserved
 * 
 * All code or information contained herein is, and remains the 
 * property of Xngage LLC and its customers.  The intellectual 
 * and technical concepts contained are proprietary to Xngage LLC 
 * and may be covered by U.S. and Foreign Patents, patents in 
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material 
 * is strictly forbidden unless prior written permission is obtained 
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\EventListener;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Psr\Log\LoggerInterface;
use Oro\Bundle\UserBundle\Entity\User;
use Xngage\CustomFeaturesBundle\Layout\DataProvider\PublicMethodsProvider;

class UserEntityListener
{
    /** @var DoctrineHelper */
    protected $doctrineHelper;

    /** @var PublicMethodsProvider */
    private $publicMethodsProvider;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /*
     * @param DoctrineHelper $doctrineHelper
     * @param PublicMethodsProvider  $publicMethodsProvider
     */
    public function __construct(
        DoctrineHelper $doctrineHelper,
        PublicMethodsProvider $publicMethodsProvider,
        LoggerInterface $logger
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->publicMethodsProvider = $publicMethodsProvider;
        $this->logger = $logger;
    }

    /** @ORM\postUpdate() */
    public function postUpdate(User $entity, LifecycleEventArgs $event)
    {
        if (!$entity->getCustomDefault()) {
            return;
        }

        $qb = $this->getEntityManager()
            ->getRepository(User::class)
            ->createQueryBuilder('user')
            ->update()
            ->set('user.custom_default', ':customDefault')
            ->where('user.id != :userId')
            ->setParameter('userId', $entity->getId())
            ->setParameter('customDefault', false)
            ->getQuery()
            ->execute();
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->doctrineHelper->getEntityManagerForClass(User::class);
    }
}