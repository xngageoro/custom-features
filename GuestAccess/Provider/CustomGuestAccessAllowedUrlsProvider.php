<?php

namespace Xngage\Bundle\CustomFeaturesBundle\GuestAccess\Provider;

use Oro\Component\Routing\UrlUtil;
use Symfony\Component\Routing\RequestContext;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\FrontendBundle\GuestAccess\Provider\GuestAccessAllowedUrlsProviderInterface;

/**
 * Provides a list of patterns for URLs for which an access is granted for non-authenticated visitors.
 */
class CustomGuestAccessAllowedUrlsProvider implements GuestAccessAllowedUrlsProviderInterface
{
    /**
     * @var string[]
     */
    private $allowedUrls = [];

    /**
     * @var RequestContext
     */
    private $requestContext;

    /** @var ConfigManager */
    private $configManager;

    /**
     * @param RequestContext $requestContext
     * @param ConfigManager $configManager
     */
    public function __construct(
        RequestContext $requestContext,
        ConfigManager $configManager
    ) {
        $this->configManager = $configManager;
        $this->requestContext = $requestContext;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllowedUrlsPatterns()
    {
        $baseUrl = $this->requestContext->getBaseUrl();
        $guestPagesConfig = $this->configManager->get('xngage_custom_features.allowed_guest_pages');
        $allowedGuestPages = preg_split('/\s+/', $guestPagesConfig);

        $allowedGuestUrls = [];
        foreach ($allowedGuestPages as $page) {
            $allowedGuestUrls[] =  UrlUtil::getPathInfo(trim($page), $baseUrl);
        }

        return \array_merge($this->allowedUrls, $allowedGuestUrls);
    }
}
