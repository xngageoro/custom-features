<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion

namespace Xngage\Bundle\CustomFeaturesBundle\Layout\DataProvider;

use Doctrine\Common\Cache\CacheProvider;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Oro\Bundle\CatalogBundle\Entity\Category;
use Xngage\CustomFeaturesBundle\Layout\DataProvider\PublicMethodsProvider;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\SecurityBundle\Authentication\TokenAccessor;

class HomepageProvider
{
    private $em;

    /**
     * @var PublicMethodsProvider
     */
    private $publicMethodsProvider;

    /** @var TokenAccessor */
    private $tokenAccessor;

    /** @var CacheProvider */
    private $cache;

    /** @var int */
    private $cacheLifeTime;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PublicMethodsProvider $publicMethodsProvider
     * @param TokenAccessor $tokenAccessor
     */
    public function __construct(
        EntityManager $entityManager,
        PublicMethodsProvider $publicMethodsProvider,
        TokenAccessor $tokenAccessor
    )
    {
        $this->publicMethodsProvider =  $publicMethodsProvider;
        $this->em = $entityManager;
        $this->tokenAccessor = $tokenAccessor;
    }

    public function getFeaturedCategoriesIds()
    {
        $user = $this->getCurrentUser();
        $cacheKey = $this->getCacheKey($user);

        $result = $this->cache->fetch($cacheKey);
        if (false !== $result) {
            return $result;
        }

        $categories = $this->em->getRepository(Category::class)->findBy(['custom_featured' => true]);
        $featured = [];
        foreach($categories as $category) {
            $featured[] = $category->getId();
        }
        
        $this->cache->save($cacheKey, $featured, $this->cacheLifeTime);

        return $featured;

    }

    public function isFeaturedCategoriesEnabled()
    {
        return 
            $this->publicMethodsProvider->getConfigParamValue('xngage_custom_features.homepage_show_featured_categories')
        &&
            count($this->getFeaturedCategoriesIds()) > 0;
    }

    public function isFeaturedProductsEnabled()
    {
        return $this->publicMethodsProvider->getConfigParamValue('xngage_custom_features.homepage_show_featured_products');
    }

    public function isNewArrivalsEnabled()
    {
        return $this->publicMethodsProvider->getConfigParamValue('xngage_custom_features.homepage_show_new_arrivals');
    }

    public function isTopSellingItemsEnabled()
    {
        return $this->publicMethodsProvider->getConfigParamValue('xngage_custom_features.homepage_show_top_selling_items');
    }

    /**
     * @param CacheProvider $cache
     * @param int           $lifeTime
     */
    public function setCache(CacheProvider $cache, $lifeTime = 0)
    {
        $this->cache = $cache;
        $this->cacheLifeTime = $lifeTime;
    }

    /**
     * @param CustomerUser|null $user
     *
     * @return string
     */
    private function getCacheKey(?CustomerUser $user): string
    {
        $customer = $user ? $user->getCustomer() : null;
        $customerGroup = $customer ? $customer->getGroup() : null;

        return sprintf(
            'xngage_featured_categories_%s_%s',
            $user ? $user->getId() : 0,
            $customerGroup ? $customerGroup->getId() : 0,
        );
    }

    /**
     * @return CustomerUser|null
     */
    private function getCurrentUser()
    {
        $tokenUser = $this->tokenAccessor->getUser();
        if ($tokenUser instanceof CustomerUser) {
            return $tokenUser;
        }

        return null;
    }
}
