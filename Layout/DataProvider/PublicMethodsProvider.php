<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Layout\DataProvider;

use Doctrine\ORM\EntityManagerInterface;
use Oro\Bundle\EmailBundle\Mailer\Processor;
use Oro\Bundle\EmailBundle\Model\EmailTemplate;
use Oro\Bundle\EmailBundle\Provider\EmailRenderer;
use Oro\Bundle\EmailBundle\Tools\EmailOriginHelper;
use Oro\Bundle\EntityBundle\Exception\EntityNotFoundException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Xngage\CustomFeaturesBundle\Entity\ApplicationLog;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\AttachmentBundle\Entity\File;
use Oro\Bundle\AttachmentBundle\Manager\AttachmentManager;
use Oro\Bundle\EmailBundle\Form\Model\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\Email as EmailConstraints;
use Symfony\Component\Validator\Exception\ValidatorException;
use Xngage\CustomFeaturesBundle\DependencyInjection\Configuration;
use Oro\Bundle\LayoutBundle\Layout\DataProvider\AssetProvider;

class PublicMethodsProvider
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ConfigManager
     */
    private $configManager;

    /**
     * @var AttachmentManager
     */
    private $attachmentManager;

    /**
     * @var Processor
     */
    private $emailProcessor;

    /**
     * @var EmailRenderer
     */
    private $renderer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var EmailOriginHelper
     */
    private $emailOriginHelper;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var AssetProvider
     */
    private $assetProvider;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ConfigManager $configManager
     * @param UrlGeneratorInterface $urlGenerator
     * @param AttachmentManager $attachmentManager
     * @param Processor $emailProcessor
     * @param EmailRenderer $renderer
     * @param ValidatorInterface $validator
     * @param EmailOriginHelper $emailOriginHelper
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ConfigManager          $configManager,
        UrlGeneratorInterface  $urlGenerator,
        AttachmentManager      $attachmentManager,
        Processor              $emailProcessor,
        EmailRenderer          $renderer,
        ValidatorInterface     $validator,
        EmailOriginHelper      $emailOriginHelper,
        AssetProvider          $assetProvider
    ) {
        $this->entityManager = $entityManager;
        $this->configManager = $configManager;
        $this->urlGenerator = $urlGenerator;
        $this->attachmentManager = $attachmentManager;
        $this->emailProcessor = $emailProcessor;
        $this->renderer = $renderer;
        $this->validator = $validator;
        $this->emailOriginHelper = $emailOriginHelper;
        $this->assetProvider = $assetProvider;
    }

    /**
     * @param string $slug
     * @return string
     */
    public function preSlugify($slug)
    {
        $slug = strtolower($slug);
        $slug = str_replace(' ', "-", $slug);
        $slug = str_replace("'", "-", $slug);
        $slug = str_replace('"', "-", $slug);
        $slug = str_replace('(', "-", $slug);
        $slug = str_replace(')', "-", $slug);
        $slug = str_replace('"', "-", $slug);
        $slug = str_replace('/', "-", $slug);
        $slug = str_replace(',', "", $slug);
        $slug = str_replace('%', "", $slug);
        $slug = str_replace('#', "", $slug);
        $slug = str_replace('&', "and", $slug);
        $slug = str_replace('=', "", $slug);
        $slug = str_replace('*', "", $slug);
        $slug = str_replace('@', "", $slug);
        $slug = str_replace(':', "-", $slug);
        $slug = str_replace('.', "-", $slug);
        $slug = str_replace('+', "", $slug);
        $slug = preg_replace('/[[:space:]]+/', '-', $slug);

        return $this->parseNonLatinChars($slug);
    }

    public function removeNonUtf8($string)
    {
        $regex = <<<'END'
                                /
                                  (
                                    (?: [\x00-\x7F]                 # single-byte sequences   0xxxxxxx
                                    |   [\xC0-\xDF][\x80-\xBF]      # double-byte sequences   110xxxxx 10xxxxxx
                                    |   [\xE0-\xEF][\x80-\xBF]{2}   # triple-byte sequences   1110xxxx 10xxxxxx * 2
                                    |   [\xF0-\xF7][\x80-\xBF]{3}   # quadruple-byte sequence 11110xxx 10xxxxxx * 3 
                                    ){1,100}                        # ...one or more times
                                  )
                                | .                                 # anything else
                                /x
                                END;

        return preg_replace($regex, '$1', $string);
    }

    private function parseNonLatinChars($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

    public function logging($type, $source, $title, $message, $flush = true)
    {
        if ($this->getConfigParamValue('xngage_custom_features.save_to_application_log')) {
            $log = new ApplicationLog();
            $log->setType($type);
            $log->setSource($source);
            $log->setTitle($title);
            $log->setMessage($message);
            $log->setCreatedAt(new \DateTime());

            $this->entityManager->persist($log);

            if ($flush) {
                $this->entityManager->flush();

                return $log->getId();
            }
        }

        return null;
    }


    public function logCriticalMessage($type, $source, $title, $message, $flush = true)
    {
        $log = new ApplicationLog();
        $log->setType($type);
        $log->setSource($source);
        $log->setTitle($title);
        $log->setMessage($message);
        $log->setCreatedAt(new \DateTime());

        $this->entityManager->persist($log);

        if ($flush) {
            $this->entityManager->flush();
            return $log->getId();
        }

        return null;
    }

    public function getConfigParamValue($name)
    {
        return $this->configManager->get($name);
    }

    public function getLogo()
    {
        $file = $this->getConfigParamValue('xngage_custom_features.logo_field');
        if ($file == '') {
            return 'default';
        } else {
            $repo = $this->entityManager->getRepository(File::class);
            $fileObj = $repo->findOneById($file);
            return $fileObj;
        }
    }

    public function getLogoPath()
    {
        $filePath = '';
        $imageId = $this->getConfigParamValue('xngage_custom_features.logo_field');

        if ($imageId && $image = $this->entityManager->getRepository(File::class)->find($imageId)) {
            /** @var File $image */
            $filePath = $this->attachmentManager->getFileUrl($image);
        }

        return $filePath;
    }


    public function getFavicon($size) {
        $fieldName = sprintf(Configuration::PREFIX_FAVICONS_FIELD, $size, $size);
        $imageId = $this->getConfigParamValue('xngage_custom_features.' . $fieldName);

        if ($imageId && $image = $this->entityManager->getRepository(File::class)->find($imageId)) {
            return $this->attachmentManager->getFilteredImageUrl($image, 'original');
        }
       
        return $this->assetProvider->getUrl("bundles/orofrontend/blank/images/favicon/favicon.ico");
    }

    public function printToConsole($output, $message)
    {
        if ($this->getConfigParamValue('xngage_custom_features.print_to_console')) {
            $output->writeln($message);
        }
    }

    /**
     * @throws EntityNotFoundException
     */
    public function sendEmail($from, $to = [], $entity = null, $template = null)
    {
        if (trim($this->getConfigParamValue('xngage_custom_features.testing_email')) != '') {
            $to = [trim($this->getConfigParamValue('xngage_custom_features.testing_email'))];
        }

        $fromEmail = $this->getConfigParamValue('oro_notification.email_notification_sender_email');

        $emailModel = $this->getEmailModel();
        $this->validateAddress($from);

        $emailModel->setFrom($fromEmail);
        $emailModel->setTo($to);

        $emailTemplate = $this->entityManager->getRepository('OroEmailBundle:EmailTemplate')->findByName($template);

        if (!$emailTemplate) {
            $errorMessage = sprintf('Template "%s" not found.', $template);
            throw new EntityNotFoundException($errorMessage);
        }

        $templateData = $this->renderer->compileMessage($emailTemplate, ['entity' => $entity]);

        list($subjectRendered, $templateRendered) = $templateData;

        $emailModel->setSubject($subjectRendered);
        $emailModel->setBody($templateRendered);
        $emailModel->setType($emailTemplate->getType());

        try {
            $this->emailProcessor->process(
                $emailModel,
                $this->emailOriginHelper->getEmailOrigin($emailModel->getFrom(), $emailModel->getOrganization())
            );
        } catch (\Swift_SwiftException $exception) {

        }
    }


    public function sendTextEmail($job_command, $body, $from = '', $to = '')
    {
        if (empty(trim($to)) || empty(trim($body))) {
            return;
        }

        if (trim($this->getConfigParamValue('xngage_custom_features.testing_email')) != '') {
            $to = trim($this->getConfigParamValue('xngage_custom_features.testing_email'));
        }

        $fromEmail = $this->getConfigParamValue('oro_notification.email_notification_sender_email');

        $source = $job_command;
        $subject = "Integration Job $job_command completed with errors";

        if (isset(debug_backtrace()[1])) {
            $source = debug_backtrace()[1]['class'] . "::" . debug_backtrace()[1]['function'] . " line " . debug_backtrace()[1]['line'];
        }

        $insertedId = $this->logCriticalMessage(ApplicationLog::INFO_TYPE, $source, $subject, $body);

        if (!$insertedId) {
            return;
        }

        $websiteUrl = $this->getConfigParamValue('oro_ui.application_url');

        $url = $this->urlGenerator->generate('application_log_view', ['id' => $insertedId]);
        $url = $websiteUrl . $url;

        $body = "Integration job $job_command has been completed with errors, please check $url for more details.";

        $emailModel = $this->getEmailModel();
        $this->validateAddress($from);

        $emailModel->setFrom($fromEmail);
        $emailModel->setTo([$to]);
        $emailModel->setSubject($subject);
        $emailModel->setBody($body);
        $emailModel->setType(EmailTemplate::CONTENT_TYPE_HTML);

        try {
            $this->emailProcessor->process(
                $emailModel,
                $this->emailOriginHelper->getEmailOrigin($emailModel->getFrom(), $emailModel->getOrganization())
            );
        } catch (\Swift_SwiftException $exception) {
            $this->logCriticalMessage(ApplicationLog::ERROR_TYPE, $source, $subject, $exception->getMessage());
        }
    }

    /**
     * @param string $email
     *
     * @throws \Symfony\Component\Validator\Exception\ValidatorException
     */
    private function validateAddress($email)
    {
        $emailConstraint = new EmailConstraints();
        $emailConstraint->message = 'Invalid email address';
        if ($email) {
            $errorList = $this->validator->validate(
                $email,
                $emailConstraint
            );
            if ($errorList && $errorList->count() > 0) {
                throw new ValidatorException($errorList->get(0)->getMessage());
            }
        }
    }

    /**
     * @return Email
     */
    private function getEmailModel()
    {
        return new Email();
    }
}
