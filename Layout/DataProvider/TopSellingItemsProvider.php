<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Layout\DataProvider;

use Oro\Bundle\ProductBundle\Provider\ProductsProviderInterface;
use Foodmaven\CoreThemeBundle\Layout\DataProvider\PublicMethodsProvider;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\OrderBundle\Entity\OrderLineItem;
use Oro\Bundle\ProductBundle\Entity\Manager\ProductManager;
use Oro\Bundle\ProductBundle\Entity\Repository\ProductRepository;
use Doctrine\Common\Cache\Cache;

/**
 * Xngage Top selling products
 */
class TopSellingItemsProvider implements ProductsProviderInterface
{
    /** @var ProductRepository */
    private $productRepository;

    /** @var ProductManager */
    private $productManager;

    /** @var DoctrineHelper */
    private $doctrineHelper;

    protected $warehouseManager;

    private $cache;

    CONST TOP_SELLING = "topselling";

    /** @var PublicMethodsProvider */
    private $publicMethodsProvider;

    public function __construct(
        ProductRepository $productRepository,
        ProductManager $productManager,
        DoctrineHelper $doctrineHelper,
        Cache $cache
    ){
        $this->productRepository = $productRepository;
        $this->productManager = $productManager;
        $this->doctrineHelper = $doctrineHelper;
        $this->cache = $cache;
    }

    public function getProducts()
    {
        $cacheKey = self::TOP_SELLING;
        $returnProducts = $this->cache->fetch($cacheKey);

        if (false === $returnProducts) {
            $qbLineItems = $this->doctrineHelper->getEntityRepository(OrderLineItem::class)->createQueryBuilder('li');
            $qbLineItems->select('IDENTITY(li.product) as product_id, COUNT(li.product) as score')
                ->where($qbLineItems->expr()->isNotNull('li.product'))
                ->groupBy('li.product')
                ->orderBy('score', 'DESC')
                ->setMaxResults(10);

            $result = $qbLineItems->getQuery()->getArrayResult();

            $productIds = [];
            foreach ($result as $item) {
                $productIds[] = $item['product_id'];
            }

            $qbProducts = $this->productRepository->getProductsQueryBuilder($productIds);
            $this->productManager->restrictQueryBuilder($qbProducts, []);
            $returnProducts = $qbProducts->getQuery()->getResult();

            $this->cache->save($cacheKey, $returnProducts, 3600); //1 hour
        }

        return $returnProducts;
    }
}
