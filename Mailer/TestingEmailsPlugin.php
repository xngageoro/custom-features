<?php
#region copyright
/*
3 * XNGAGE CONFIDENTIAL
4 * __________________________
5 * 
6 * Copyright (C) 2021 Xngage - All Rights Reserved
7 * 
8 * All code or information contained herein is, and remains the 
9 * property of Xngage LLC and its customers.  The intellectual 
10 * and technical concepts contained are proprietary to Xngage LLC 
11 * and may be covered by U.S. and Foreign Patents, patents in 
12 * process, and are protected by trade secret or copyright law.  
13 * Dissemination of this information or reproduction of this material 
14 * is strictly forbidden unless prior written permission is obtained 
15 * from Xngage LLC.
16 */
#endregion
namespace Xngage\Bundle\CustomFeaturesBundle\Mailer;

use Swift_Events_SendEvent;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Xngage\CustomFeaturesBundle\DependencyInjection\XngageCustomFeaturesExtension;

class TestingEmailsPlugin implements \Swift_Events_SendListener
{

    /** @var ConfigManager */
    private $configManager;

    /**
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSendPerformed(Swift_Events_SendEvent $evt)
    {
        $testEmail = $this->configManager->get(XngageCustomFeaturesExtension::ALIAS . '.testing_email');
        if (!empty($testEmail)) {
            $message = $evt->getMessage();
            $message->setCc([]);
            $message->setBcc([]);
            $message->setTo([$this->configManager->get(XngageCustomFeaturesExtension::ALIAS . '.testing_email')]);
        }
    }

    /**
     * Not used.
     * {@inheritDoc}
     */
    public function sendPerformed(Swift_Events_SendEvent $evt)
    {
    }
}
