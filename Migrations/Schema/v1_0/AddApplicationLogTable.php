<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion

namespace Xngage\Bundle\CustomFeaturesBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtension;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtensionAwareInterface;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;


class AddApplicationLogTable implements Migration,ExtendExtensionAwareInterface
{

    const TABLE_NAME = 'xngage_application_log';

    /** @var ExtendExtension */
    protected $extendExtension;

    /**
     * Sets the ExtendExtension
     *
     * @param ExtendExtension $extendExtension
     */
    public function setExtendExtension(ExtendExtension $extendExtension)
    {
        $this->extendExtension = $extendExtension;
    }
    /**
     * Modifies the given schema to apply necessary changes of a database
     * The given query bag can be used to apply additional SQL queries before and after schema changes
     *
     * @param Schema $schema
     * @param QueryBag $queries
     * @return void
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /** Tables generation **/
        $this->createApplicationLogTable ($schema);

    }

    private function createApplicationLogTable(Schema $schema)
    {
        if (!$schema->hasTable(self::TABLE_NAME))
        {
            $table = $schema->createTable(self::TABLE_NAME);

            if (!$table->hasColumn('id') ) {
                $table->addColumn('id', 'integer', ['autoincrement' => true]);
            }

            if (!$table->hasColumn('type') ) {
                $table->addColumn('type', 'string', ['notnull' => false]);
            }

            if (!$table->hasColumn('source') ) {
                $table->addColumn('source', 'string', ['notnull' => false]);
            }

            if (!$table->hasColumn('title') ) {
                $table->addColumn('title', 'string', ['notnull' => false]);
            }

            if (!$table->hasColumn('message') ) {
                $table->addColumn('message', 'text', ['notnull' => false]);
            }

            if (!$table->hasColumn('created_at') ) {
                $table->addColumn('created_at', 'datetime', ['notnull' => true]);
            }

            if ($table->hasColumn('id') ) {
                $table->setPrimaryKey(['id']);
            }
        }
    }
}