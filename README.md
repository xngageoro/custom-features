Features in our customFeaturesBundle:

1- ApplicationLog table
2- Commands:
  a. Clean up ApplicationLog
  b. Chmod Commands
  c. Clean up stuck jobs
  d. Indexing products
3- Controllers:
  a. ApplicationLog
  b. Special Commands Tool (indexing, chmod, clean up job)
  c. Query Tool
  d. Uom
  e. Platform (show platform file logs)
4- Backoffice default user
5- Redirect to referral URL after login
6- Allow configurable URLs to be accessable in guest mode
7- Testing Email
8- Logout after configurable time of period
9- Custom Twig functions(like validation on url)
10- Configurable parameters:
  a. Logo
  b. Fav icons
  c. Jobs scheduled time (clean up jobs, reindex, ..etc.)
  d. Corporate address
  e. print to console for integration jobs
  f. show/hide sliders in home page (featured catetories, newal arrival, featured products, top selling)
  g. Batch size for integration jobs
