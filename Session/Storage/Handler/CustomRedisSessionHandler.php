<?php
namespace Xngage\Bundle\CustomFeaturesBundle\Session\Storage\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Oro\Bundle\RedisConfigBundle\Session\Storage\Handler\RedisSessionHandler;
use Predis\Client;

class CustomRedisSessionHandler extends RedisSessionHandler
{
    /**
     * Redis session storage constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param Client|\Redis $redis Redis database connection
     * @param array $options Session options
     * @param string $prefix Prefix to use when writing session data
     * @param bool $locking Indicates an sessions should be locked
     * @param int $spinLockWait Microseconds to wait between acquire lock tries
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        $redis,
        array $options = array(),
        $prefix = 'session',
        $locking = true,
        $spinLockWait = 150000
    ) {
        parent::__construct($redis, $options, $prefix, $locking, $spinLockWait);

        $enableQuery = "select text_value from oro_config_value where name = 'session_cookie_lifetime_enable_field'";
        $conn1 = $entityManager->getConnection()->executeQuery($enableQuery);
        $enableArray = $conn1->fetchAllNumeric();

        $enable = 1;

        if (isset($enableArray[0][0])) {
            $enable = $enableArray[0][0];
        }

        if ($enable) {
            $timeoutQuery = "select text_value from oro_config_value where name = 'session_cookie_lifetime_field'";
            $conn2 = $entityManager->getConnection()->executeQuery($timeoutQuery);
            $timeArray = $conn2->fetchAllNumeric();

            $time = 120;
            if (isset($timeArray[0][0])) {
                $time = $timeArray[0][0];
            }
            
            $timeInSec = $time * 60;
            $this->setTtl($timeInSec);
            $this->ttl = $timeInSec;

            ini_set('session.gc_maxlifetime', $timeInSec);
            //ini_set('session.cookie_lifetime', $timeInSec);
        }
    }
}