<?php

namespace Xngage\Bundle\CustomFeaturesBundle\Twig;

use Oro\Bundle\ProductBundle\Entity\Product;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class PublicTwigExtension extends AbstractExtension {
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('validUrl', array($this, 'validUrl')),
        );
    }

    public function validUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

}