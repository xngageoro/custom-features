<?php

namespace Xngage\Bundle\CustomFeaturesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Xngage\CustomFeaturesBundle\DependencyInjection\Compiler\SwiftMailerHandlerPass;

class XngageCustomFeaturesBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new SwiftMailerHandlerPass());
    }
}
